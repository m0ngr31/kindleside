@echo off & setLocal EnableDELAYedExpansion

rd /S/Q medium>null 2>&1
rd /S/Q small>null 2>&1
rd /S/Q books>null 2>&1
rd /S/Q coverart>null 2>&1
rd /S/Q .palmkindle\coverCache>null 2>&1
rd /S/Q .palmkindle>null 2>&1

mkdir medium
mkdir small
mkdir .palmkindle
mkdir .palmkindle\coverCache

del temp*.txt>null 2>&1
set "asin1=B00"
set /a count=0

echo         KindleSide: Quick and dirty script to prepare kindle 
echo         compatible files to be loaded to your HP Touchpad.
echo.
echo ****  Special thanks goes to tompe (from MobileRead forums) for 
echo ****  the mobi2mobi tool, Kevin Bosch for his imaze resizer,
echo ****  and last but not least kjhenrie (for making Kindle-Import),
echo ****  and nj (for figuring out how to extract the book length)
echo ****  for making it possible to do all of this!
echo.

for /f %%a IN ('dir /s /b *.mobi') do (set /a count=count+1)
for /f %%a IN ('dir /s /b *.azw') do (set /a count=count+1)
echo Number of files: %count%

set /a diff=((9999-%count%) - 1000) + 1
set /a divisor = 32767 / %diff%
set /a diff2=(999 - 100) + 1
set /a divisor2 = 32767 / %diff2%

:Random1
set /a asin2=%random% / %divisor%
if /i %asin2% GTR 9999 goto Random1
if /i %asin2% LSS 1000 goto Random1

:Random2
set /a asin3=%random% / %divisor2%
if /i %asin3% GTR 999 goto Random2
if /i %asin3% LSS 100 goto Random2
set /a temp1=!asin2!

for /f "delims=  " %%b IN ('dir /s /b *.mobi') do (

echo Processing %%b...
mobi2mobi.exe "%%b" > temp1.txt
findstr /i "longtitle" temp1.txt > temp2.txt
set /p title1=<temp2.txt
set title1=!title1:~11!
set title1=!title1::=!

find /i "item: 100" temp1.txt > temp3.txt

for /f "tokens=* delims= " %%a in (temp3.txt) do (
set var=%%a
)

set author1=!var!
set author1=!author1:~33!
if "!author1:~0,1!"==" " (set author1=!author1:~1!)

set filename=%%b

dd bs=1 count=2 skip=0x50 if="!filename!" of=offset.bin>null 2>&1
bin2hex offset.bin offset.txt>null 2>&1
set /p hexstr= < offset.txt
set hexstr=!hexstr:~9,4!

set /a offset=0x!hexstr! + 4
dd bs=1 count=4 skip=!offset! if="!filename!" of=tloc.bin>null 2>&1
bin2hex tloc.bin tloc.txt>null 2>&1
set /p hexstr2= < tloc.txt
set hexstr2=!hexstr2:~9,8!

set /a aztloc=0x!hexstr2!/150
set /a aztloc+=1

if "!tempfile!"=="!filename!" goto :end

mobi2mobi.exe --outfile ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi" --delexthtype 113 "!filename!">null 2>&1
mobi2mobi.exe --outfile ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi" --delexthtype 501 ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi">null 2>&1
mobi2mobi.exe --outfile ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi" --exthtype 113 --exthdata "!asin1!!asin3!!temp1!" ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi">null 2>&1
mobi2mobi.exe --outfile ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi" --addexthtype 501 --exthdata "EBOK" ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi">null 2>&1
mobi2mobi.exe --savethumb "medium\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!-medium.jpg" "!filename!">null 2>&1
mobi2mobi.exe --savethumb "small\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!-small.jpg" "!filename!">null 2>&1
echo Done processing !title1!

set /a temp1=!temp1!+1
set tempfile=!filename!

del temp*.txt>null 2>&1
del offset.txt>null 2>&1
del offset.bin>null 2>&1
del tloc.txt>null 2>&1
del tloc.bin>null 2>&1
del null*
)

for /f "delims=  " %%b IN ('dir /s /b *.azw') do (

echo Processing %%b...
mobi2mobi.exe "%%b" > temp1.txt
findstr /i "longtitle" temp1.txt > temp2.txt
set /p title1=<temp2.txt
set title1=!title1:~11!
set title1=!title1::=!

find /i "item: 100" temp1.txt > temp3.txt

for /f "tokens=* delims= " %%a in (temp3.txt) do (
set var=%%a
)

set author1=!var!
set author1=!author1:~33!

set filename=%%b

dd bs=1 count=2 skip=0x50 if="!filename!" of=offset.bin>null 2>&1
bin2hex offset.bin offset.txt>null 2>&1
set /p hexstr= < offset.txt
set hexstr=!hexstr:~9,4!

set /a offset=0x!hexstr! + 4
dd bs=1 count=4 skip=!offset! if="!filename!" of=tloc.bin>null 2>&1
bin2hex tloc.bin tloc.txt>null 2>&1
set /p hexstr2= < tloc.txt
set hexstr2=!hexstr2:~9,8!

set /a aztloc=0x!hexstr2!/150
set /a aztloc+=1

if "!tempfile!"=="!filename!" goto :end

mobi2mobi.exe --outfile ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi" --delexthtype 113 "!filename!">null 2>&1
mobi2mobi.exe --outfile ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi" --delexthtype 501 ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi">null 2>&1
mobi2mobi.exe --outfile ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi" --exthtype 113 --exthdata "!asin1!!asin3!!temp1!" ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi">null 2>&1
mobi2mobi.exe --outfile ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi" --addexthtype 501 --exthdata "EBOK" ".palmkindle\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!.mobi">null 2>&1
mobi2mobi.exe --savethumb "medium\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!-medium.jpg" "!filename!">null 2>&1
mobi2mobi.exe --savethumb "small\!title1! - !author1! - !asin1!!asin3!!temp1! - !aztloc!-small.jpg" "!filename!">null 2>&1
echo Done processing !title1!

set /a temp1=!temp1!+1
set tempfile=!filename!

del temp*.txt>null 2>&1
del offset.txt>null 2>&1
del offset.bin>null 2>&1
del tloc.txt>null 2>&1
del tloc.bin>null 2>&1
del null*
)

:end
CA.Console.ResizeImage.exe "-s:medium" "-d:.palmkindle\coverCache" "-h:170" "-w:120">null 2>&1
CA.Console.ResizeImage.exe "-s:small" "-d:.palmkindle\CoverCache" "-h:74" "-w:52">null 2>&1
rd /S/Q medium
rd /S/Q small
del null*
